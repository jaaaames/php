<?php 
	require "../partials/template.php";
	function get_body_contents(){

	}

 ?>
 	<div class="container">
 		<h1 class="col-lg-4">LOGIN</h1>
 		<div class="row">
 			<form action="../controllers/login-process.php" method="POST">
 				<div class="form-group">
 					<label for="email">Email</label>
 					<input type="email" name="email" class="form-control">
 				</div>

 				<div class="form-group">
 					<label for="password">Password</label>
 					<input type="password" name="password" class="form-control">
 				</div>

 				<div class="form-group">
 					<button class="btn btn-info" type="submit">Submit</button>
 				</div>
 			</form>
 		</div>
 	</div>