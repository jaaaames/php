CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	firstname VARCHAR(255) NOT NULL,
	lastname VARCHAR(255) NOT NULL,
	birthdate DATE NOT NULL,
	contactnumber VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (ticket_user)
		ON UPDATE CASCADE
		ON DELETE SET NULL,
	FOREIGN KEY (item_user)
		ON UPDATE CASCADE
		ON DELETE SET NULL
);

CREATE TABLE categories (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (category_ticket)
		ON UPDATE CASCADE
		ON DELETE SET NULL
);

CREATE TABLE priorities (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY (id)
	FOREIGN KEY (priority_ticket)
		ON UPDATE CASCADE
		ON DELETE SET NULL
);

CREATE TABLE items (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	description VARCHAR(255) NOT NULL,
	date DATE NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY ()
);

CREATE TABLE tickets (
	id INT NOT NULL AUTO_INCREMENT,
	title VARCHAR(255) NOT NULL,
	message TEXT NOT NULL,
	category_id INT,
	priority_id INT,
	item_id INT,
	PRIMARY KEY (id),
	FOREIGN KEY (category_id)
		REFERENCES (categories_id)
		ON UPDATE CASCADE
		ON DELETE SET NULL,
	FOREIGN KEY (priority_id)
		REFERENCES (priorities_id)
		ON UPDATE CASCADE
		ON DELETE SET NULL,
	FOREIGN KEY (item_id)
		REFERENCES (items_id)
		ON UPDATE CASCADE
		ON DELETE SET NULL,
);

--------------------------------------

CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	firstName VARCHAR(255) NOT NULL,
	lastName VARCHAR(255) NOT NULL,
	birthdate VARCHAR(255) NOT NULL,
	contactNumber VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
	ticket_id INT,
	item_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(ticket_id)
		REFERENCES tickets(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL,
	FOREIGN KEY(item_id)
		REFERENCES items(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL
);

CREATE TABLE tickets (
	id INT NOT NULL AUTO_INCREMENT,
	title VARCHAR(255) NOT NULL,
	message VARCHAR(255) NOT NULL,
	category_id INT,
	priority_id INT,
	item_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY (category_id)
		REFERENCES categories(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL,
	FOREIGN KEY (priority_id)
		REFERENCES priorities(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL,
	FOREIGN KEY (item_id)
		REFERENCES items(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL
);

CREATE TABLE categories (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE priorities (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE items (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	description VARCHAR(255) NOT NULL,
	date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id)
);


