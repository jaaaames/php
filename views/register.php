<<?php 
	require "../partials/template.php";
	function get_body_contents(){
?>
	<div class="row">
		<div class="col-lg-4 offset-lg-4">
			<form action="../controllers/register-process.php" method="POST">
				<div class="form-group">
					<label for="firstName">First Name:</label>
					<input type="text" name="firstName" class="form-control">
					<label for="lastName">Last Name:</label>
					<input type="text" name="lastName" class="form-control">
				</div>
				<div class="form-group">
					<label for="email">Email:</label>
					<input type="email" name="email" class="form-control">
				</div>
				<div class="form-group">
					<label for="password">Password:</label>
					<input type="password" name="password" class="form-control">
				</div>
				<div class="text-center">
					<button class="btn  btn-info" type="submit">Register</button>
				</div>
			</form>
		</div>
	</div>




 <?php
}
?>