//steps we well take ;
//we need to capture all the add to cart button;
//we need to add an event listener to each of the buttons;
//we need to get the data (the id) from the buttton;
//we need to get the quantity of the input;
//we need to check if the quantity > 0;
//if yes, send the data to the controller via... fetch;



//we need to capture all the add to cart button;
let addToCartBtns = document.querySelectorAll(".addToCart");
// console.log(addToCartBtns);

//we need to add an event listener to each of the buttons;
addToCartBtns.forEach(indiv_btn=>{
	indiv_btn.addEventListener("click", btn=>{
		//we need to get the data (the id) from the buttton;
		let id = btn.target.getAttribute("data-id");
		let quantity = btn.target.previousElementSibling.value
		if(quantity <= 0){
			alert("Please enter valid quantity");
		}else{
			let data = new FormData;
			data.append("id", id); //first parameter is name of property(id), what to send
			data.append("cart", quantity);

			fetch("../../controllers/add-to-cart-process.php",{
				method:"POST",
				body: data
			}) //method and what to send
			.then(response=>{
				return response.text();
			})
			.then(res=>{
				document.getElementById('cartCount').textContent = res;
			})
		}
	})
})
