let student = 'James';
student = 'Vincent';

const age = 18;

//let - variable declaration, values can be changed;
//const - variable declaration, values can't be changed;
//var has a scoping issue; var, instead of only existing where it was defined, it can be accessed outside


// for(var i = 0; i < 5; i++){
// 	console.log(i);
// }

// console.log(i);

// function sayMyName(name){
// 	alert("Hi " + name);
// }

// sayMyName("Brandon");

const sayMyName = name => {
	alert("Hi " + name);
}
// sayMyName("James");

const ages = [21,22,23,24,25]

ages.forEach(function(age){
	console.log(age);
})

ages.forEach(age=>{
	console.log(age);
})







