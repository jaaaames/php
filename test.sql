CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

ALTER TABLE artists DROP COLUMN name;
ALTER TABLE artists ADD COLUMN name VARCHAR(255) NOT NULL;

INSERT INTO albums(name, year, artist_id) VALUES('Trip', '1996', 1);

SELECT * FROM songs WHERE genre = "k-pop" or length < 250;

SELECT * FROM employees WHERE 

UPDATE songs SET length = 240 WHERE title = 'Kundiman';

DELETE FROM songs WHERE lengh > 240;

--USE FROM from the part without foreign id first
SELECT name, title FROM albums JOIN songs ON (albums.id = songs.album_id);

INSERT INTO tableA(a1, a2) VALUES ('A', 1), ('B', 3), ('C', 3), ('D', 4), ('E', 4);
INSERT INTO tableB(b1, tableA_id) VALUES ('V', 5), ('W', 4), ('X', 3), ('Y', 2), ('Z', 1);
SELECT * FROM tableB JOIN tableA ON (tableB.id = tableB.tableB_id);


..to filter results
1. LIKE
...
WHERE <columnName> LIKE <pattern>;
!!!!!!!
_ -> 1 character;
% -> any number of characters;

1. ALL songs that start with letter K;

SELECT * FROM songs
WHERE title LIKE "k%";
Kundiman

SELECT * FROM songs
WHERE title LIKE "%n%";
Kundiman

SELECT * FROM songs
WHERE title LIKE "__n";
Kundiman
Gangnam Style

SELECT * FROM songs
WHERE title NOT LIKE "%k%";
Gangnam Style

--if case sensitive
BINARY
SELECT * FROM songs
WHERE title LIKE BINARY "%K%";
Kundiman
Kisapmata

--looking for two words
SELECT * FROM songs
WHERE title LIKE BINARY "% %";

--looking for three words
SELECT * FROM songs
WHERE title LIKE BINARY "% % %";

SELECT * FROM starwars ORDER BY year;
SELECT * FROM starwars ORDER BY year DESC;
SELECT * FROM starwars ORDER BY year, star;
SELECT * FROM starwars ORDER BY year DESC, star DESC;
SELECT DISTINCT star FROM starwars; 
SELECT * FROM starwars ORDER BY year, star DESC;
SELECT DISTINCT year, title, star FROM starwars;
SELECT DISTINCT year FROM starwars WHERE star = 'Carrie Fisher';

SELECT * FROM artists WHERE screen_name NOT LIKE "%s" ORDER BY birthday DESC; 

SELECT screen_name, studios.name FROM artists 
JOIN studios ON (artists.studio_id=studios.id) 
ORDER BY screen_name DESC;

SELECT name FROM producers 
WHERE name LIKE "%m*"

SELECT producers.name, title, genre, length FROM producers 
JOIN movie_producer ON (producers.id=movie_producer.producer_id) 
JOIN movies ON (movie_producer.movie_id=movies.id) 
WHERE producers.name LIKE "%m%" ORDER BY length;


SELECT producers.name, title FROM producers 
JOIN movie_producer ON (producers.id=movie_producer.producer_id) 
JOIN movies ON (movie_producer.movie_id=movies.id) 
WHERE producers.name LIKE BINARY "P%";


COUNT()
SUM()
MIN()
MAX()
AVG()

---

GROUPING RESULTS

select length, genre from songs group by genre;

select avg(length), genre from songs group by genre;

---
HAVING clause

select avg(length), genre from songs group by genre having avg(length)> 250;

join delete multiple subquery group by

avg lengths of movies per studio


SELECT title from movies where id=(movie_id from artist_id where id=(artist_id from ))


SELECT AVG(LENGTH), studios.name FROM artists
JOIN studios ON (artists.studio_id = studios.id)
JOIN artist_movie ON (artists.id = artist_movie.artist_id)
JOIN movies ON (artist_movie.movie_id = movies.id)
GROUP BY studios.name;


--1
SELECT id FROM ranks WHERE name = 'junior';
--final answer
SELECT firstName, lastName, branches.name AS branchName FROM employees
JOIN branch_employee ON (employees.id = branch_employee.employee_id);
JOIN branches ON (branches.id = branch_employee.branch_id)
WHERE rank_id = (SELECT id FROM ranks WHERE name = 'junior')
	AND id IN (SELECT id FROM branches
JOIN branch_employee






SELECT id FROM branches 
JOIN branch_employee ON (branches.id = branch_employee.branch_id);


WHERE name IN('QC', 'Ortigas');

CREATE TABLE xxxxx (
	id INT NOT NULL AUTO_INCREMENT,

)

CREATE TABLE payments (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
)

CREATE TABLE statuses (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
)

CREATE TABLE addresses (
	id INT NOT NULL AUTO_INCREMENT,
	address1 VARCHAR(255) NOT NULL,
	address2 VARCHAR(255),
	city VARCHAR(255) NOT NULL,
	zipCode INT,
	isPrimary BOOLEAN,
	user_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(user_id)
		REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL
)

CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	firstName VARCHAR(255) NOT NULL,
	lastName VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	password VARCHAR (255) NOT NULL,
	role_id INT
	PRIMARY KEY(id)
	FOREIGN KEY(role_id)
		REFERENCES roles(id)
		ON DELETE RESTRICT
		ON UPDATE CASCADE
)

CREATE TABLE contacts (
	id INT NOT NULL AUTO_INCREMENT,
	contactNo VARCHAR(255),
	user_id INT,
	isPrimary BOOLEAN,
	PRIMARY KEY(id),
	FOREIGN KEY (user_id)
		REFERENCES users(id)
		ON DELETE RESTRICT
		ON UPDATE CASCADE
)

CREATE TABLE xxxxx (
	id INT NOT NULL AUTO_INCREMENT,

)

CREATE TABLE orders (
	id INT NOT NULL AUTO_INCREMENT,
	total INT,
	user_id INT,
	status_id INT,
	payment_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(user_id)
		REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
	FOREIGN KEY(status_id)
		REFERENCES statuses(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
	FOREIGN KEY(payment_id)
		REFERENCES payments(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT	
)

CREATE TABLE items (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	price INT NOT NULL,
	description VARCHAR(255) NOT NULL,
	imgPath VARCHAR(255),
	category_id INT,
	PRIMARY KEY(id)
	FOREIGN KEY(category_id)
		REFERENCES categories(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
)

CREATE TABLE item_order (
	id INT NOT NULL AUTO_INCREMENT,
	item_id INT,
	order_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(item_id)
		REFERENCES items(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
	FOREIGN KEY(order_id)
		REFERENCES orders(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
)