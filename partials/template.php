<!DOCTYPE html>
<html>
<head>
	<title></title>

	<!-- Bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/lux/bootstrap.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg bg-info">
		  <a class="navbar-brand" href="../index.php">Sumsang Gadgets</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarColor02">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="catalog.php">Dumbphones<span class="sr-only">(current)</span></a>
		      </li>
		      <?php 
		      session_start();
		      if (isset($_SESSION['email']) && $_SESSION['email'] =="admin@admin.com") {
		       	# code...
		       ?>
		       		<li class="nav-item">
		        	<a class="nav-link" href="add-items.php">Add Item</a>
		      </li>
		       <?php
		       } else{
		       	?>
		       		<li class="nav-item">
		        	<a class="nav-link" href="cart.php">Cart <span class="badge bg-warning">
		        		<?php 
		        			if (isset($_SESSION['cart'])){
							echo array_sum($_SESSION['cart']);
						}else{
							echo 0;
						}
						?>

		        	</span></a>
		      		</li>
		      	<?php	
		       }

		       ?>
		      <?php 
		      		if(isset($_SESSION['firstName'])){
		      		?>	
		      		<li class="nav-item">
		        	<a class="nav-link" href="#">Hello <?php echo $_SESSION['firstName']?>!</a>
		      		</li>

		      		<li class="nav-item">
		        	<a class="nav-link" href="../controllers/logout-process.php">Logout</a>
		      		</li>
		      <?php			
		      	}else{
		      ?>
		      	<li class="nav-item">
		        	<a class="nav-link" href="login.php">Login</a>
		      	</li>
		      	<li class="nav-item">
		        	<a class="nav-link" href="register.php">Register</a>
		      	</li>
		      <?php
		      	}
		      ?>
		      <li class="nav-item">
		        <a class="nav-link" href="#">About</a>
		      </li>
		    </ul>
		  </div>
		</nav>

		<!-- Page Contents -->
		<?php get_body_contents() ?>

		<!-- Footer -->
		<footer class="page-footer font-small bg-dark navbar-dark">
			<div class="footer-copyright text-center py-3">2020 Made with love B55</div>
		</footer>
</body>
</html>