//fetch().then().then()
//first .then() => transforms the data into a readable format
//second.then() => what we will do with the data we received
//without fetch, the function below will run without letting the fetch finish first

let users = [];

fetch("https://jsonplaceholder.typicode.com/users")
  .then(response_from_fetch => { //naming the response from fetch, any name can be used, most used[res];
    return response_from_fetch.json()
  })
.then(data_from_response => {
    users = data_from_response;
})


// const sayMyName(nameFromFetch)=>{

// }

fetch("https://api.nasa.gov/planetary/apod?api_key=8wZ2JrPTSSzIeic6HD4xscMLJVPhYvb7AyKBl03G")
  .then(res=>res.json())
  .then(res=>{
    document.getByElementID('nasaPhoto').src = res.url;
    document.getByElementID('nasaPhoto').nextElementSibling.innerText = res.title;
    document.getByElementID('nasaPhoto').nextElementSibling.nextElementSibling.innerText = res.date;
    document.getByElementID('nasaPhoto').nextElementSibling.nextElementSibling.nextElementSibling.innerText = res.explanation;
  })  
