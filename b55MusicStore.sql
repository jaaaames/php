CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE artists (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE albums (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	year YEAR(4),
	artist_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(artist_id)
	REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE CASCADE
);

CREATE TABLE songs (
	id INT NOT NULL AUTO_INCREMENT,
	title VARCHAR(255) NOT NULL,
	length INT,
	genre VARCHAR(255),
	album_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(album_id)
		REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL
);

CREATE TABLE playlists (
	id INT NOT NULL AUTO_INCREMENT,
	date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	user_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(user_id)
		REFERENCES users(id)
			ON UPDATE CASCADE
			ON DELETE SET NULL	
);

CREATE TABLE playlist_song (
	id INT NOT NULL AUTO_INCREMENT,
	song_id INT,
	playlist_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(song_id)
		REFERENCES songs(id)
			ON UPDATE CASCADE
			ON DELETE SET NULL,
	FOREIGN KEY(playlist_id)
		REFERENCES playlists(id)
			ON UPDATE CASCADE
			ON DELETE SET NULL
);

ALTER TABLE artists DROP COLUMN name;

ALTER TABLE artists ADD COLUMN name VARCHAR(255) NOT NULL;

-- create a ticket
-- title
-- category 
-- priority (one category low/medium/high) : name
-- item assigned to me
-- message
-- users firstname, lastname, birthdate, contact number, email, password
-- users | ticket | category
-- ticket > category (one to many)
-- priority > ticket (one to many)
-- item (name description date)
-- category (name)
-- priority(name)
-- message
-- users (one)
-- ticket (one)

-- Database name b55irs

-- CREATE
-- INSERT


-- CRUD --
INSERT INTO tableName (ColumnName1, ColumnName2...) VALUES (valueOfColumnName1, valueOfColumnName2...)

INSERT INTO artists(name) VALUES('Rivermaya');
INSERT INTO artists(name) VALUES('Psy');

-- RETRIEVE
-- SELECT
-- SELECT * FROM <tableName>;

SELECT * FROM artists;

--SELECT * FROM <tableName> WHERE <condition>;
SELECT * FROM artists WHERE id =2;

--SELECT <columnName> FROM <tableName> WHERE <condition>;

--INSERT INTO artists(name) VALUES('');
INSERT INTO artists(name) VALUES('Rivermaya');
INSERT INTO artists(name) VALUES('Psy');

-- INSERT INTO albums(name, year, artist_id) VALUES();
INSERT INTO albums(name, year, artist_id) VALUES('Psy6', '2012', 2);
INSERT INTO albums(name, year, artist_id) VALUES('Trip', '1996', 1);

INSERT INTO albums(name, year, artist_id) VALUES('Lover', '2019', 3);

-- INSERT INTO songs('title', 'length', 'genre', album_id) VALUES();
INSERT INTO songs(title, length, genre, album_id) VALUES('Gangnam Style', 253, 'k-pop', 1);
INSERT INTO songs(title, length, genre, album_id) VALUES('Kundiman', 234, 'OPM', 2);
INSERT INTO songs(title, length, genre, album_id) VALUES('Kisapmata', 279, 'OPM', 2);

--SELECTING DATA
SELECT * FROM songs WHERE genre = "k-pop" or length < 250;

--UPDATING DATA
UPDATE tableName SET ColumnName1 = newValue, ColumnName2 = newValue ... WHERE columnName = value;
UPDATE songs SET length = 240 WHERE title = 'Kundiman';

--DELETING DATA
DELETE FROM <tableName> WHERE column = value;
DELETE FROM songs WHERE lengh > 240;

--JOINS (we can only join tables that have a relationship)
--USE FROM from the part without foreign id first
SELECT <column/s> FROM table1 JOIN table2 ON (joinCondition) WHERE condition;
SELECT name, title FROM albums JOIN songs ON (albums.id = songs.album_id);

SELECT name, name FROM artists JOIN albums ON (artists.id = albums.artist_id);
SELECT artists.name AS artist, albums.name AS album FROM artists JOIN albums ON (artists.id = albums.artist_id);

-----------------------------------------------------------------------------------
1. Create Table "tableA"
	id;
	a1 VARCHAR(255);
	a2 INT;

2. Create Table "tableB"
	id;
	b1 VARCHAR(255);
	tableA_id (foreign key of TableA);
-------------------------
CREATE TABLE tableA (
	id INT NOT NULL AUTO_INCREMENT,
	a1 VARCHAR(255),
	a2 INT,
	PRIMARY KEY(id)
);

CREATE TABLE tableB (
	id INT NOT NULL AUTO_INCREMENT,
	b1 VARCHAR(255),
	tableA_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY (tableA_id)
		REFERENCES tableA(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL
);

INSERT INTO tableA(a1, a2) VALUES ('A', 1), ('B', 3), ('C', 3), ('D', 4), ('E', 4);
INSERT INTO tableB(b1, tableA_id) VALUES ('V', 5), ('W', 4), ('X', 3), ('Y', 2), ('Z', 1);

SELECT * FROM tableB JOIN tableA ON (tableB.id = tableB.tableB_id);

--------------------------------------------------

CREATE TABLE movies (
	id INT NOT NULL AUTO_INCREMENT,
	title VARCHAR(255) NOT NULL,
	year YEAR(4),
	length INT,
	genre VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE studios (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	address VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE producers (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	address VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE artists (
	id INT NOT NULL AUTO_INCREMENT,
	screen_name VARCHAR(255) NOT NULL,
	address VARCHAR(255) NOT NULL,
	gender VARCHAR(255) NOT NULL,
	birthday DATE NOT NULL,
	studio_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(studio_id)
		REFERENCES studios(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL
);

CREATE TABLE movie_producer (
	id INT NOT NULL AUTO_INCREMENT,
	producer_id INT,
	movie_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(producer_id)
		REFERENCES producers(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL,
	FOREIGN KEY(movie_id)
		REFERENCES movies(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL
);

CREATE TABLE artist_movie (
	id INT NOT NULL AUTO_INCREMENT,
	movie_id INT,
	artist_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(movie_id)
		REFERENCES movies(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL,
	FOREIGN KEY(artist_id)
		REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL
);

INSERT INTO movies(title, year, length, genre) VALUES('Hello, Love, Goodbye', 2019, 7020, 'romance'), ('One More Chance', 2000, 8400, 'drama'), ('Kakabakaba ka ba?', 1980, 6240, 'thriller'), ('Barumbado', 1990, 7456, 'action'), ("Sa 'Yo Lamang", 2019, 7020, 'romance');
INSERT INTO studios(name, address) VALUES('ABS-CBN', "Eugenio Lopez Dr, Diliman, Quezon City, Metro Manila"), ('GMA', "GMA International, 10F GMA Network Center, EDSA cor, Timog Ave, Diliman, Quezon City, 1103 Metro Manila"), ('TV5', "Reliance St, Mandaluyong, Metro Manila"), ('RPN', "Broadcast  City, 1119 Capitol Hills Dr, Diliman, Quezon City, Metro Manila");
INSERT INTO producers(name, address) VALUES('Erik Matti', "Ching Building 2345 Taft Avenue 1300, Pasay City, Philippines"), ('Joyce Jimenez', "3 N. Fifth Ave. Fort Walton Beach, FL 32547"), ('Lily Monteverde', "9970 Newbridge Ave. Blackwood, NJ 08012"), ('Lav Diaz', "9325 Rockwell Ave. Memphis, TN 38106");
INSERT INTO artists(screen_name, address, gender, birthday, studio_id) VALUES ('Alden Richards', '121 Thomas Ave. Petersburg, VA 23803', 'bicurious', '1992-01-02', 2), ('Bea Alonzo', '142 E. Greystone Drive Johnson City, TN 37601', 'asexual', '1987-10-17', 1), ('Christopher de Leon', '135 Sulphur Springs St. Nazareth, PA 18064', 'heterosexual', '1956-10-31', 3), ('Robin Padilla', '94 Cedar Swamp Rd. Seymour, IN 47274', 'heterosexual', '1969-11-23', 1);
INSERT INTO movie_producer(producer_id, movie_id) VALUES (1, 1), (1, 2), (2, 1), (3, 4), (3, 3), (4, 5);
INSERT INTO artist_movie(movie_id, artist_id) VALUES (1, 1), (2, 2), (3, 3), (4, 4), (5, 2), (5, 3);

SELECT * FROM movies WHERE genre = 'drama' AND year > 1990;
SELECT * FROM artists WHERE gender = 'heterosexual' AND studio_id = 1;
SELECT * FROM producers;
UPDATE producers SET address = '9510 Shirley St. Logansport, IN 46947' WHERE name = 'Erik Matti';
UPDATE artists SET gender = 'heterosexual';
DELETE FROM studios WHERE name = 'RPN';

-----------------------------------------------

..to filter results
1. LIKE
...
WHERE <columnName> LIKE <pattern>;
!!!!!!!
_ -> 1 character;
% -> any number of characters;

1. ALL songs that start with letter K;

SELECT * FROM songs
WHERE title LIKE "k%";
Kundiman

SELECT * FROM songs
WHERE title LIKE "%n%";
Kundiman

SELECT * FROM songs
WHERE title LIKE "__n";
Kundiman
Gangnam Style

SELECT * FROM songs
WHERE title NOT LIKE "%k%";
Gangnam Style

--if case sensitive
BINARY
SELECT * FROM songs
WHERE title LIKE BINARY "%K%";
Kundiman
Kisapmata

--looking for two words
SELECT * FROM songs
WHERE title LIKE BINARY "% %";

--looking for three words
SELECT * FROM songs
WHERE title LIKE BINARY "% % %";



-----------------------------------------

CREATE TABLE starwars (
	id INT NOT NULL AUTO_INCREMENT,
	title VARCHAR(255) NOT NULL,
	year YEAR(4),
	star VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

INSERT INTO starwars(title, year, star) VALUES ('Star Wars', 1977, 'Carrie Fisher'), ('Star Wars', 2015, 'Carrie Fisher'), ('Star Wars', 2015, 'Daisy Ridley'), ('Star Wars', 1977, 'Harrison Ford'), ('Star Wars', 2015, 'Harrison Ford');

--SORTING INFORMATION
ORDER BY

SELECT * FROM starwars ORDER BY year;
MariaDB [b55Practice]> SELECT * FROM starwars ORDER BY year;
+----+-----------+------+---------------+
| id | title     | year | star          |
+----+-----------+------+---------------+
|  1 | Star Wars | 1977 | Carrie Fisher |
|  4 | Star Wars | 1977 | Harrison Ford |
|  2 | Star Wars | 2015 | Carrie Fisher |
|  3 | Star Wars | 2015 | Daisy Ridley  |
|  5 | Star Wars | 2015 | Harrison Ford |
+----+-----------+------+---------------+
5 rows in set (0.01 sec)


--descending order
SELECT * FROM starwars ORDER BY year DESC;
MariaDB [b55Practice]> SELECT * FROM starwars ORDER BY year DESC;
+----+-----------+------+---------------+
| id | title     | year | star          |
+----+-----------+------+---------------+
|  2 | Star Wars | 2015 | Carrie Fisher |
|  3 | Star Wars | 2015 | Daisy Ridley  |
|  5 | Star Wars | 2015 | Harrison Ford |
|  1 | Star Wars | 1977 | Carrie Fisher |
|  4 | Star Wars | 1977 | Harrison Ford |
+----+-----------+------+---------------+
5 rows in set (0.00 sec)

MariaDB [b55Practice]> 

--sorting using 2 parameters
SELECT * FROM starwars ORDER BY year, star;
MariaDB [b55Practice]> SELECT * FROM starwars ORDER BY year, star;
+----+-----------+------+---------------+
| id | title     | year | star          |
+----+-----------+------+---------------+
|  1 | Star Wars | 1977 | Carrie Fisher |
|  4 | Star Wars | 1977 | Harrison Ford |
|  2 | Star Wars | 2015 | Carrie Fisher |
|  3 | Star Wars | 2015 | Daisy Ridley  |
|  5 | Star Wars | 2015 | Harrison Ford |
+----+-----------+------+---------------+
5 rows in set (0.00 sec)

--sorting using 2 parameters, one parameter descending
SELECT * FROM starwars ORDER BY year, star DESC;
MariaDB [b55Practice]> SELECT * FROM starwars ORDER BY year, star DESC;
+----+-----------+------+---------------+
| id | title     | year | star          |
+----+-----------+------+---------------+
|  4 | Star Wars | 1977 | Harrison Ford |
|  1 | Star Wars | 1977 | Carrie Fisher |
|  5 | Star Wars | 2015 | Harrison Ford |
|  3 | Star Wars | 2015 | Daisy Ridley  |
|  2 | Star Wars | 2015 | Carrie Fisher |
+----+-----------+------+---------------+
5 rows in set (0.00 sec)

--sorting using 2 parameters, both parameters descending
SELECT * FROM starwars ORDER BY year DESC, star DESC;
MariaDB [b55Practice]> SELECT * FROM starwars ORDER BY year DESC, star DESC;
+----+-----------+------+---------------+
| id | title     | year | star          |
+----+-----------+------+---------------+
|  5 | Star Wars | 2015 | Harrison Ford |
|  3 | Star Wars | 2015 | Daisy Ridley  |
|  2 | Star Wars | 2015 | Carrie Fisher |
|  4 | Star Wars | 1977 | Harrison Ford |
|  1 | Star Wars | 1977 | Carrie Fisher |
+----+-----------+------+---------------+
5 rows in set (0.00 sec)


--unique
DISTINCT

SELECT DISTINCT <distinct column> 
SELECT DISTINCT star FROM starwars; 
MariaDB [b55Practice]> SELECT DISTINCT star FROM starwars; 
+---------------+
| star          |
+---------------+
| Carrie Fisher |
| Daisy Ridley  |
| Harrison Ford |
+---------------+
3 rows in set (0.01 sec)


SELECT DISTINCT year FROM starwars;
SELECT DISTINCT year, title, star FROM starwars;

MariaDB [b55Practice]> SELECT DISTINCT year, title, star FROM starwars;
+------+-----------+---------------+
| year | title     | star          |
+------+-----------+---------------+
| 1977 | Star Wars | Carrie Fisher |
| 2015 | Star Wars | Carrie Fisher |
| 2015 | Star Wars | Daisy Ridley  |
| 1977 | Star Wars | Harrison Ford |
| 2015 | Star Wars | Harrison Ford |
+------+-----------+---------------+
5 rows in set (0.00 sec)

SELECT DISTINCT year FROM starwars WHERE star = 'Carrie Fisher';
MariaDB [b55Practice]> SELECT DISTINCT year FROM starwars WHERE star = 'Carrie Fisher';
+------+
| year |
+------+
| 1977 |
| 2015 |
+------+
2 rows in set (0.00 sec)


------------------------------

SELECT * FROM artists WHERE screen_name NOT LIKE "%s" ORDER BY birthday DESC; 

SELECT screen_name, studios.name FROM artists JOIN studios ON (artists.studio_id=studios.id) ORDER BY screen_name DESC;

SELECT name FROM producers WHERE name LIKE "%m*"

SELECT producers.name, title, genre, length FROM producers JOIN movie_producer ON (producers.id=movie_producer.producer_id) JOIN movies ON (movie_producer.movie_id=movies.id) WHERE producers.name LIKE "%m%" ORDER BY length;


SELECT producers.name, title FROM producers JOIN movie_producer ON (producers.id=movie_producer.producer_id) JOIN movies ON (movie_producer.movie_id=movies.id) WHERE producers.name LIKE BINARY "P%";


--Sub Queries:
----------------------------
What is the address of Bea Alonzo's studio?'

MariaDB [b55Practice]> SELECT * FROM studios WHERE id = (SELECT studio_id FROM artists WHERE screen_name = 'Bea Alonzo');
+----+---------+------------------------------------------------------+
| id | name    | address                                              |
+----+---------+------------------------------------------------------+
|  1 | ABS-CBN | Eugenio Lopez Dr, Diliman, Quezon City, Metro Manila |
+----+---------+------------------------------------------------------+
1 row in set (0.00 sec)

IN = OR

SELECT size, price FROM tbl_shirts WHERE id IN (3,1);
same as
SELECT size, price FROM tbl_shirts WHERE id = 3 or id=1;

SELECT name FROM artists WHERE id IN
(SELECT artist_id FROM albums WHERE id IN
(SELECT album_id FROM songs WHERE genre IN ('OPM', 'rock', 'k-pop')));

--ANY matches one of the given values;

select shirt_id from tbl_materials  WHERE material = 'cotton';
select price from tbl_shirts where id in


get the prices and sizes of shirts that are more expensive than all of those made of cotton;
select size, price from shirts where price > ALL
(select price from shirts where id in
(select shirt_id from materials where material='cotton'));

shirts
id size price
1 s 450
2 m 500
3 l 550
4 xl 600

materials
id material shirt_id
1 cotton 3
2 cotton 1
3 chino 4
4 denim 2

--------------------------------------------
aggregation and grouping


SELECT * from starwars where title='star wars' and year = 2015;

MariaDB [b55Practice]> SELECT * from starwars where title='star wars' and year = 2015;
+----+-----------+------+---------------+
| id | title     | year | star          |
+----+-----------+------+---------------+
|  2 | Star Wars | 2015 | Carrie Fisher |
|  3 | Star Wars | 2015 | Daisy Ridley  |
|  5 | Star Wars | 2015 | Harrison Ford |
+----+-----------+------+---------------+
3 rows in set (0.00 sec)

---

SELECT COUNT(*) from starwars where title='star wars' and year = 2015;

MariaDB [b55Practice]> SELECT COUNT(*) from starwars where title='star wars' and year = 2015;
+----------+
| COUNT(*) |
+----------+
|        3 |
+----------+
1 row in set (0.00 sec)


COUNT()
SUM()
MIN()
MAX()
AVG()

---

GROUPING RESULTS

select length, genre from songs group by genre;

select avg(length), genre from songs group by genre;

---
HAVING clause

select avg(length), genre from songs group by genre having avg(length)> 250;

join delete multiple subquery group by

avg lengths of movies per studio


SELECT title from movies where id=(movie_id from artist_id where id=(artist_id from ))


SELECT AVG(LENGTH), studios.name FROM artists
JOIN studios ON (artists.studio_id = studios.id)
JOIN artist_movie ON (artists.id = artist_movie.artist_id)
JOIN movies ON (artist_movie.movie_id = movie.id)
GROUP BY studios.name;

select * from employees JOIN branch_employee ON (employees.id=branch_employee.employee_id) WHERE branch_id=1 OR branch_id=3;

select employees set salary +=  where id=(select employee_id from department_employee where department_id=(select id from departments where id = 2 or id = 4));